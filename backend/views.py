from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import Backend
from .serializers import BackendSerializer,GetAllBackendSerializer
# Create your views here.
 

class GetAllBackendAPIView(APIView):
    def get(self, request):
        list_backend = Backend.objects.all()
        mydata = GetAllBackendSerializer(list_backend, many=True)
        return Response(data=mydata.data, status= status.HTTP_200_OK)


    def post(self, request):
        mydata = BackendSerializer(data=request.data)
        if not mydata.is_valid():
            return Response('Data is wrong', status=status.HTTP_400_BAD_REQUEST)
        nameproduct = mydata.data['nameproduct']
        price = mydata.data['price']
        postAPI = Backend.objects.create(nameproduct= nameproduct, price=price)
        return Response(data=postAPI.id, status=status.HTTP_200_OK)
    

class BackendInfor(APIView):
    def get(self,request,id):
        try:
            obj = Backend.objects.get(id=id)
        except Backend.DoesNotExist:
            msg ={"msg":"Not Found"}
            return Response(msg, status=status.HTTP_400_NOT_FOUND)
        mydata = BackendSerializer(obj)
        return Response(mydata.data, status=status.HTTP_200_OK)
    
    def put(self,request,id):
        try:
            obj = Backend.objects.get(id=id)
        except Backend.DoesNotExist:
            msg ={"msg":"Not Found"}
            return Response(msg, status=status.HTTP_400_NOT_FOUND)
        mydata = BackendSerializer(obj, data=request.data)

        if mydata.is_valid():
            mydata.save()
            return Response(mydata.data, status=status.HTTP_205_RESET_CONTENT)
        return Response(mydata.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def patch(self,request,id):
        try:
            obj = Backend.objects.get(id=id)
        except Backend.DoesNotExist:
            msg ={"msg":"Not Found"}
            return Response(msg, status=status.HTTP_400_NOT_FOUND)
        mydata = BackendSerializer(obj, data=request.data, partial=True)

        if mydata.is_valid():
            mydata.save()
            return Response(mydata.data, status=status.HTTP_205_RESET_CONTENT)
        return Response(mydata.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def delete(self,request,id):
        try:
            obj = Backend.objects.get(id=id)
        except Backend.DoesNotExist:
            msg ={"msg":"Not Found"}    
            return Response(msg, status=status.HTTP_400_NOT_FOUND)
        obj.delete()
        return Response({"msg":"deleted"}, status=status.HTTP_204_NO_CONTENT)

            



