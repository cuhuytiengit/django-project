from rest_framework import serializers
from .models import Backend



class GetAllBackendSerializer(serializers.ModelSerializer):
    class Meta:
        model = Backend
        fields = ('id', 'nameproduct', 'price')



class BackendSerializer(serializers.Serializer):
    nameproduct =  serializers.CharField(max_length=250)
    price =  serializers.IntegerField()