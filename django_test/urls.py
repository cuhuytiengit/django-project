from django.contrib import admin
from django.urls import path,include
from backend.views import GetAllBackendAPIView,BackendInfor
urlpatterns = [
    path('website/', include('website.urls')),


    path('admin/', admin.site.urls),
    path('backend/', GetAllBackendAPIView.as_view()),
    path('backend/<int:id>/', BackendInfor.as_view()),
]
