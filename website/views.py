from django.template import loader
from django.shortcuts import render
from django.http import HttpResponse
import requests,urllib3
import collections 
import sys
if sys.version_info.major == 3 and sys.version_info.minor >= 10:
    from collections.abc import MutableMapping
else:
    from collections import MutableMapping


def index(request):
    product_list = requests.get('http://hostnametest.viewdns.net:8001/backend/').json()
    context = {'product_data' : product_list}
    return render(request,'myfirst.html', context)
